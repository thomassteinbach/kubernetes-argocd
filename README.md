# Xarif ArgoCD deployments

## preparations - sops age key

Create SOPS Age key (or recover it from keepass)

```bash
mkdir ~/.sops
age-keygen -o ~/.sops/key.txt
chmod 0700 ~/.sops
chmod 0600 ~/.sops/key.txt
echo 'export SOPS_AGE_KEY_FILE=$HOME/.sops/key.txt' >> ~/.zshrc # or ~/.bashrc
```

Upload the SOPS Age key as K8s Secret:

```bash
kubectl create secret generic argocd-sops-age-key-file --from-file=key.txt=${HOME}/.sops/key.txt -n argocd
```

## initial deployment

Install ArgoCD from this repository.
This command must eventually be executed twice, because on first execution
there will be CRD installed used by the second execution.

```bash
kubectl create namespace argocd
kustomize build argocd --enable-helm | kubectl apply -f -
kustomize build argocd-apps --enable-helm | kubectl apply -f -
```

## connect to ArgoCD

Run/inspect the script [port-forward-argocd.sh](port-forward-argocd.sh)
