#!/usr/bin/env bash

rm -rf argocd/charts
kustomize build --enable-helm argocd