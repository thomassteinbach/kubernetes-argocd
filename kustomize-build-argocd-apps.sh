#!/usr/bin/env bash

rm -rf argocd-apps/charts
kustomize build --enable-helm argocd-apps