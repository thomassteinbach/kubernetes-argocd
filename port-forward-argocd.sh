#!/usr/bin/env bash

echo "Login with username 'admin' and password:"
kubectl get secret argocd-initial-admin-secret -n argocd -o jsonpath={.data.password} | base64 -d; echo
echo "to https://localhost:8080"
echo -e "\nStarting port forward..."
kubectl port-forward service/argocd-server -n argocd 8080:80